# Box2DWrapper
This is a wrapper for the boxwd library, it's quickly made, and probably not the best, but yeah, it'll do.

NOTE: This git repository uses submoduels so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!

NOTE: Since this repository uses submodules DO NOT download it using the web interface it does not download submodules instead clone the project with the arguments: --recurse-submodules -j8 !!!



 - Download and compile OpenGLWrapper and OpenGLWrapperUtils.

  Note: Be sure to remove -master from the folders names, make sure all the projects including this are in the same directory.
  

#  How to build the release (build release library binary)
  Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and the default package manager to get libraries and install stuff):
  - Arch: ./automatron9000/automatron9000.sh -m release -o arch -l "$(cat Libraries/linux/arch/ilibs)"


# How to test build (build debug executable binary)
 Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - Arch: ./automatron9000/automatron9000.sh -m debug -o arch
