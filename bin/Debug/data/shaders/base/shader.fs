//Set the current version so that opengl knows which version of the compiler to use to compile this
#version 330 core

//Input
//Note this time we don't get the vertices attribute since when we give the data we only give it to the vertex shader
//But what we do need to make is the uniform that we will use
uniform sampler2D objectTexture;
uniform int useColor;
uniform int r,g,b;

//Shader variables
in vec2 pass_tex_coord;
//Output
out vec4 FragmentColor;

void main() {
   //Set the texture of the quad
    if(useColor == 1){
    FragmentColor = texture(objectTexture, pass_tex_coord) * vec4( r/255, g/255, b/255, 1);
    }else{
    FragmentColor = texture(objectTexture, pass_tex_coord);
    }

}
