//Set the current version so that opengl knows which version of the compiler to use to compile this
#version 330 core

//Input
//Get the variables from the Shader class that compiled it
//Note: it must have the same name as the name placed in the Shader class
layout(location=0) in vec2 positions;
layout(location=1) in vec2 tex_coord;

//Output
//Shader variables
out vec2 pass_tex_coord;

//Make a uniform for other to set
uniform mat3 transformationMatrix;
uniform mat3 cameraProjection;

void main() {
   //For fragment shader
   pass_tex_coord = tex_coord;
   //This is what we render
   gl_Position =  vec4( (cameraProjection * transformationMatrix ) * vec3(positions, 1), 1);
   
}
