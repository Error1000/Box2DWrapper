#ifndef WORLD_H
#define WORLD_H

#include <Box2D/Box2D.h>

class World{
private:
 static b2World* world;

public:
 inline static void init(b2Vec2 gravity = b2Vec2(0.0f, -9.8f)){
   World::world = new b2World(gravity);
 }

 inline static void cleanup(){
   delete World::world;
 }

 inline static b2World* getWorld(){
   return World::world;
 }

};

#endif
