#include "PhysicalObject.h"

PhysicalObject::PhysicalObject(b2Vec2 pos, b2PolygonShape* shape, float density, float friction, float restitution, b2BodyType type){
b2BodyDef def;
def.position.Set(pos.x, pos.y);
def.type = type;

PhysicalObject::ownBody = World::getWorld()->CreateBody(&def);

b2FixtureDef fixture;
fixture.shape = shape;
fixture.density = density;
fixture.friction = friction;
fixture.restitution = restitution;

PhysicalObject::ownBody->CreateFixture(&fixture);
}

PhysicalObject::PhysicalObject(b2Vec2 pos, b2Vec2 size, float density, float friction, float restitution, b2BodyType type){
b2BodyDef def;
def.position.Set(pos.x, pos.y);
def.type = type;

PhysicalObject::ownBody = World::getWorld()->CreateBody(&def);

b2PolygonShape shape;
shape.SetAsBox(size.x, size.y);

b2FixtureDef fixture;
fixture.shape = &shape;
fixture.density = density;
fixture.friction = friction;
fixture.restitution = restitution;

PhysicalObject::ownBody->CreateFixture(&fixture);
}
