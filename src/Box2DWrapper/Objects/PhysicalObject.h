#ifndef PHYSICAL_OBJECT_H
#define PHYSICAL_OBJECT_H
#include "../Public/World.h"

class PhysicalObject{
protected:
 b2Body* ownBody;

public:
    explicit PhysicalObject(b2Vec2 pos, b2Vec2 size, float density, float friction, float restitution = 0.0f, b2BodyType type = b2_dynamicBody);
    explicit PhysicalObject(b2Vec2 pos, b2PolygonShape* shape, float density, float friction, float restitution = 0.0f, b2BodyType type = b2_dynamicBody);
    inline explicit PhysicalObject(b2Body* body){ PhysicalObject::ownBody = body; }

    inline ~PhysicalObject(){ World::getWorld()->DestroyBody( PhysicalObject::ownBody ); }


    inline const float getRotation() const{ return PhysicalObject::ownBody->GetAngle(); }
    inline const b2Vec2 getPosition() const{ return PhysicalObject::ownBody->GetPosition(); }
    inline b2Body* getBody() const{ return PhysicalObject::ownBody; }
};


#endif
