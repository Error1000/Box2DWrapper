#ifndef CONVERSIONS_H
#define CONVERSIONS_H
#include <Box2D/Box2D.h>
#include "Utils.h"


namespace Conversions{

static b2Vec2 maxCoord, minCoord;

inline void setMappingBounds(const b2Vec2 minCoordinate, const b2Vec2 maxCoordinate){
 Conversions::maxCoord = maxCoordinate;
 Conversions::minCoord = minCoordinate;
}


//TODO: There are some problems here with transforming as can be seen in TestingGraphics project, sohuld investigate this but right now it's not a priority
inline const b2Vec2 mapPixelsSizeToBox2d(const b2Vec2 siz){
return b2Vec2(map(siz.x, 0, Conversions::maxCoord.x, 0, 5)/2, map(siz.y, 0, Conversions::maxCoord.y, 0, 5)/2);
}

inline const b2Vec2 mapPixelsPosToBox2d(const b2Vec2 pos){
return b2Vec2(map(pos.x, Conversions::minCoord.x, Conversions::maxCoord.x, 0, 10), map(pos.y, Conversions::minCoord.y, Conversions::maxCoord.y, 0, 10));
}

inline const b2Vec2 mapBox2dToPixelsPos(const b2Vec2 box2dPos){
return b2Vec2(map(box2dPos.x, 0, 10, Conversions::minCoord.x, Conversions::maxCoord.x), map(box2dPos.y, 0, 10, Conversions::minCoord.y, Conversions::maxCoord.y));
}


}

#endif
