#include <OpenGLWrapper/Include.h>
#include <OpenGLWrapperUtils/Include.h>
#include "../../Box2DWrapper/Include.h"

using namespace UserUtils;

void setup();
void render();
void onExit();


int main(){
 try{
  Window::setWidth(400);
  Window::setHeight(400);
  Window::setTitle("Test");
  Wrapper::initWrapper(setup, render, onExit);
 }catch(WindowError e){
  Logger::log(e.message, Level::Error);
  return 0;
 }catch(APIError a){
  Logger::log(a.message, Level::Error);
  return 0;
 }

 Wrapper::runWrapper();
}

Texture* stickman;
Shader* defaultShader;
TexturedShape* defaultShape;
DrawableObject2D* obj;
DrawableObject2D* groundObj;

PhysicalObject* objPhys;
PhysicalObject* groundPhys;

mat3 projection;

void setup(){
  backgroundColor(135, 206, 250);
  static string vertexShader, fragmentShader;
  try{
   vertexShader = readFile(getCurrentProgramDir()+"/data/shaders/base/shader.vs");
  }catch(APIError e){
   Logger::log("Couldn't read vertex shader file!", Level::Error);
   exitWrapper();
   return;
  }

  try{
  fragmentShader = readFile(getCurrentProgramDir()+"/data/shaders/base/shader.fs");
  }catch(APIError e){
   Logger::log("Couldn't read fragment shader file!", Level::Info);
   exitWrapper();
   return;
  }

  try{
   defaultShader = new Shader(vertexShader, fragmentShader);
   defaultShader->loadUniformId("transformationMatrix");
   defaultShader->loadUniformId("cameraProjection");
   defaultShader->loadUniformId("objectTexture");
   defaultShader->loadUniformId("useColor");
   defaultShader->loadUniformId("r");
   defaultShader->loadUniformId("g");
   defaultShader->loadUniformId("b");
   defaultShader->loadAttributeId("positions");
   defaultShader->loadAttributeId("tex_coord");

  }catch(ShaderError s){
   Logger::log(s.message, Level::Error);
   exitWrapper();
   return;
  }


  defaultShader->setUniform(defaultShader->getUniformId("useColor"), 0);


  defaultShape = new TexturedRectShape(defaultShader->getAttributeId("positions"), defaultShader->getAttributeId("tex_coord"));

  stickman = new Texture( readImage(getCurrentProgramDir()+"/data/resources/textures/stickman.png"));

  obj = new DrawableObject2D(defaultShape, defaultShader);
  groundObj = new DrawableObject2D(defaultShape, defaultShader);

  obj->setSize(vec2(100, 100));
  obj->setPosition(vec2(0, 0));


  groundObj->setSize(vec2(1000, 40));
  groundObj->setPosition(vec2(0, -(Window::getHeight()/2-groundObj->getSize().y/2) ));



  defaultShape->bindShape();
  defaultShader->bindShader();

  World::init();

  projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2);
  Conversions::setMappingBounds(b2Vec2(-Window::getWidth()/2 ,-Window::getHeight()/2), b2Vec2(Window::getWidth()/2, Window::getHeight()/2));

  b2Vec2 objSiz = Conversions::mapPixelsSizeToBox2d( b2Vec2(obj->getSize().x, obj->getSize().y));
  b2Vec2 objPos = Conversions::mapPixelsPosToBox2d( b2Vec2(obj->getPosition().x, obj->getPosition().y));
  objPhys = new PhysicalObject(objPos, objSiz, 0.3f, 0.3f);

  b2Vec2 boxCoord = Conversions::mapPixelsPosToBox2d(b2Vec2(groundObj->getPosition().x, groundObj->getPosition().y));
  b2Vec2 boxSiz = Conversions::mapPixelsSizeToBox2d(b2Vec2(groundObj->getSize().x, groundObj->getSize().y));
  groundPhys = new PhysicalObject(boxCoord, boxSiz, 1.0f, 0.7f, 0.0f, b2_staticBody);

  try{
   defaultShader->validateShader();
  }catch(ShaderError validationError){
   Logger::log(validationError.message, Level::Error);
   Logger::log("OpenGL shader validation error!");
   return;
  }

}

unsigned int toWait = 0;

void render(){

  if(Window::isKeyPressed(GLFW_KEY_ESCAPE))exitWrapper();

  if(Window::getFPS() != 0) World::getWorld()->Step(1.0/Window::getFPS(), 6, 2);

  b2Vec2 gPos = Conversions::mapBox2dToPixelsPos(groundPhys->getPosition());
  groundObj->setPosition(vec2(gPos.x, gPos.y));

  b2Vec2 stickPos = Conversions::mapBox2dToPixelsPos(objPhys->getPosition());
  obj->setPosition(vec2(stickPos.x, stickPos.y));

  if(Window::isKeyPressed(GLFW_KEY_A))objPhys->getBody()->ApplyForceToCenter(b2Vec2(-objPhys->getBody()->GetMass()*(Window::getFPS()/1.0)*0.1f, 0.0f), true);
  if(Window::isKeyPressed(GLFW_KEY_D))objPhys->getBody()->ApplyForceToCenter(b2Vec2(objPhys->getBody()->GetMass()*(Window::getFPS()/1.0)*0.1f, 0.0f), true);
  if(Window::isKeyPressed(GLFW_KEY_SPACE)){
    if(toWait < 1){
        objPhys->getBody()->ApplyForceToCenter(b2Vec2(0.0f, objPhys->getBody()->GetMass()*(Window::getFPS()/1.0)*4.0f), true);
        toWait = Window::getFPS()*1.2f;
    }
  }

  if(toWait > 0)toWait--;


  defaultShader->setMatrixUniform(defaultShader->getUniformId("cameraProjection"), projection * Camera2D::getViewMatrix() );

  stickman->bindTexture(0);
  defaultShader->setUniform(defaultShader->getUniformId("useColor"), 0);
  defaultShader->setUniform(defaultShader->getAttributeId("objectTexture"), 0);
  obj->getShader()->setMatrixUniform(defaultShader->getUniformId("transformationMatrix"), obj->getModelMatrix());
  obj->renderObject();

  stickman->unbindTexture();


  defaultShader->setUniform(defaultShader->getUniformId("useColor"), 1);
  defaultShader->setUniform(defaultShader->getUniformId("r"), 255);
  defaultShader->setUniform(defaultShader->getUniformId("g"), 255);
  defaultShader->setUniform(defaultShader->getUniformId("b"), 255);
  groundObj->getShader()->setMatrixUniform(defaultShader->getUniformId("transformationMatrix"),  groundObj->getModelMatrix());
  groundObj->renderObject();

}


void onExit(){
delete stickman;
delete defaultShader;
delete defaultShape;
delete obj;
delete groundObj;

delete objPhys;
delete groundPhys;
World::cleanup();
}
